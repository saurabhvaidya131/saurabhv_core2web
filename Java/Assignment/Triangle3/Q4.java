import java.util.*;

class Q4{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);

		int row = sc.nextInt();
		int ch = (row*(row+1))/2+64;

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				System.out.print((char)ch + " ");
				ch--;
			}
			System.out.println();
		}
	}
}



