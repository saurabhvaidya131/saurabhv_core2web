class P2{
	public static void main(String s[]){
		char grade = 'A';

		switch(grade){
			case 'O':
				System.out.println("Outstanding");
				break;
			case 'A':
                                System.out.println("Excellent");
                                break;
			case 'B':
                                System.out.println("Good");
                                break;
			case 'C':
                                System.out.println("Average");
                                break;
			case 'D':
                                System.out.println("Poor");
                                break;
			default:
				System.out.println("Failed");
		}
	}
}

