class P8{
	public static void main(String s[]){
		int num1 = 21;
		int num2 = 22;
		int mult;

		if(num1>0 && num2>0){
			mult = num1*num2;

			if(mult%2==0){
				System.out.println(mult + " is an even number");
			}
			else{
				System.out.println(mult + " is an odd number");
			}
		}
		else{
			System.out.println("Sorry negative numbers not allowed");
		}
	}
}

