import java.util.*;

class Q1{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("num = ");
		int num = sc.nextInt();

		if(num%2==0){
			System.out.println(num + " is an even number");
		}
		else{
			System.out.println(num+" is an odd number");
		}
	}
}

