import java.util.*;

class Q2{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your age = ");
		int age = sc.nextInt();

		if(age>=0){
			if(age>=18){
				System.out.println("Voter is eligible for voting");
			}
			else{
				System.out.println("Voter not eligible for voting");
			}
		}
		else{
			System.out.println("Invalid age");
		}
	}
}

