import java.util.*;

class Q5{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("num = ");
		int num = sc.nextInt();

		if(num%16==0){
			System.out.println(num+" is present in the table of 16");
		}
		else{
			System.out.println(num+" is not present in the table of 16");
		}
	}
}

