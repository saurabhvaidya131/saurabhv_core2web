class IfelseDemo{
	public static void main(String s[]){
		int age = 45;
		System.out.println("Start Code");

		if(age >= 18){
			System.out.println("Eligible");
		}
		else{
			System.out.println("Not Eligible for vote");
		}
		System.out.println("End Code");
	}
}
