class ElseifDemo{
	public static void main(String s[]){
		int num = 8;
		System.out.println("Start Code");

		if(num >0){
			System.out.println("positive");
		}
		else if(num < 0){
			System.out.println("negative");
		}
		else{
			System.out.println("zero");
		}
		System.out.println("End Code");
	}
}
