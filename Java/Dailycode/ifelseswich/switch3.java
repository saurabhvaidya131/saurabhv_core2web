class Switch3{
	public static void main(String s[]){
		int num = 5;
		System.out.println("before switch");

		switch(num){
			case 1:
				System.out.println("one");

			case 2:
				System.out.println("two");

			case 3:
			        System.out.println("three");

			default :
			         System.out.println("In default state");
		}
                System.out.println("after switch");
	}
}	
