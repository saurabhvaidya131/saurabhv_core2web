class SwitchDemo{
	public static void main(String s[]){
		int num = 2;
		System.out.println("before switch");

		switch(num){
			case 1:
				System.out.println("one");
				break;

			case 2:
			        System.out.println("Two");
				break;

			case 3:
			        System.out.println("three");
			        break;

			default :
			        System.out.println("In default state");
		}
	         System.out.println("after switch");
	}
}	
