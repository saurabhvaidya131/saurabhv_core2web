class SwitchDemo3{
	public static void main(String s[]){
		String friend = "kanha";

		System.out.println("before switch");

		switch(friend){
			case "Ashish":
				System.out.println("Barclays");
				break;

			case "Rahul":
			        System.out.println("Infosys");
			        break;

			case "kanha":
			        System.out.println("IBM");
			        break;

		        default :
			        System.out.println("In default state");
		}
	}
}	
