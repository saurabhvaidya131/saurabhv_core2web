class SwitchDemo2{
	public static void main(String s[]){
		int data = 'B';
		System.out.println("before switch");

		switch(data){
			case 'A':
				System.out.println("A");
				break;

			case 'a':
			        System.out.println("a");
				break;

			case 'B':
			        System.out.println("B");
				break;


			case 'b':
			        System.out.println("b");
				break;

			default :
				System.out.println("In default state");
		}
		System.out.println("after switch");
	}
}

