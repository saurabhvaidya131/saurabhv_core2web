import java.util.*;

class A9{
        public static void main(String s[]){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter array size: ");
                int size = sc.nextInt();
		int sum = 0;

                int arr[] = new int[size];

                System.out.println("Size of an Array: "+arr.length);

                for(int i=0; i<arr.length; i++){
                        System.out.print("Enter element: ");
                        arr[i] = sc.nextInt();
                }
                System.out.println("Element are: ");

                for(int i=0; i<arr.length; i++){
                        System.out.println(arr[i]);
                }
		for(int i=0; i<arr.length; i++){
			sum = sum + arr[i];
		}
		System.out.println("Sum : "+sum);
        }
}
