class Up3{
	public static void main(String s[]){
		int x = 7;

		System.out.println(++x); //8
		System.out.println(--x); //7
		System.out.println(x--); //7
		System.out.println(x++); //6
		System.out.println(--x); //6
		System.out.println(x++); //6
		System.out.println(++x); //8
	}
}
