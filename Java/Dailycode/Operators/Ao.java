class Ao{
	public static void main(String s[]){
		int x = 20;
		int y = 10;
                x += y;                // x=x+y 
		System.out.println(x); //30

		y -= x;                 // y=y-x
		System.out.println(y);  //-20

		x *= y;                 //x=x*y
		System.out.println(x);  //-600

		y *= x;                 // y=y*x
		System.out.println(y);  //12000
	}
}
