class Bitwise{
	public static void main(String s[]){
		int x = 10;   //0000 1010
		int y = 12;   //0000 1100

		System.out.println(x&y); //0000 1000  -->8
		System.out.println(x|y); //0000 1110  -->14
	}
}
