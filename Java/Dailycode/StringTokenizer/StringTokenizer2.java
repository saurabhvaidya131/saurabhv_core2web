import java.util.*;

class StringTokenizer2{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Player Name");
		String info = sc.nextLine();

		StringTokenizer st = new StringTokenizer(info, " ");

		String str1 = st.nextToken();
		String str2 = st.nextToken();
		String str3 = st.nextToken();
		String str4 = st.nextToken();

		System.out.println("Player Name: "+str1);
		System.out.println("JerNo: "+str2);
		System.out.println("Avg Name: "+str3);
		System.out.println("Grade: "+str4);
	}
}
