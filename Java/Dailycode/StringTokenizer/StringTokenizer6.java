import java.util.*;

class StringTokenizer6{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Player name");
		String info = sc.nextLine();

		StringTokenizer st = new StringTokenizer(info, "@#");

		while(st.hasMoreTokens()){
			System.out.println(st.nextToken());
		}
	}
}

