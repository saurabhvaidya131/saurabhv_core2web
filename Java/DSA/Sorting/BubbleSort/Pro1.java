class BubbleSort{

	int fun(int arr[]){
		int count =0;
		for(int i=0; i<arr.length; i++){

			for(int j=0; j<arr.length-1; j++){
				count++;
				if(arr[j] > arr[j+1]){

					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i]+" " );
		}
		return count;
	}
	public static void main(String s[]){
		BubbleSort obj = new BubbleSort();
		int arr[] = new int[]{7,3,9,4,2,5,6};

		int ret = obj.fun(arr);
		System.out.println();
		System.out.println(ret);
	}
}

