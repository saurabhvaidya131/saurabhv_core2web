import java.util.*;

class Q3{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter row: ");
		int row = sc.nextInt();
		int space = 0;
		int col = 0;

		for(int i=1; i<row*2; i++){
			
			if(i<=row){
				space=row-i;
				col=i*2-1;
			}
			else{
				space = i-row;
				col=col-2;
			}
			int num = row;
			for(int sp=1; sp<=space; sp++){
				System.out.print(" "+"\t");
				num--;
			}
			for(int j=1; j<=col; j++){
				if(j<i-1){
					System.out.print(num-- +"\t");
				}
				else{
					System.out.print(num++ +"\t");
				}

			}
			System.out.println();
		}
	}
}


