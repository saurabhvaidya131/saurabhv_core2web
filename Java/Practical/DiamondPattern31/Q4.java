import java.util.*;

class Q10{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter row: ");
		int row = sc.nextInt();
		int Space = 0;
		int col = 0;

		for(int i=1; i<row*2; i++){

			if(i<=row){
				Space=row-i;
				col=i*2-1;
			}
			else{
				Space=i-row;
				col-=2;
			}
			for(int sp=1; sp<=Space; sp++){
				System.out.print(" "+"\t");

			}
			int num=1;
			char ch='A';
			for(int j=1; j<=col; j++){
				if(j%2==0){
					if(j<col/2+1){
						System.out.print(ch++ + "\t");
					}
					else{
						System.out.print(--ch +"\t");
				
					}
				}
				else{
					if(j<col/2+1){
						System.out.print(num++ +"\t");
					}
					else{
						System.out.print(--num +"\t");
					}
				}
			}
			System.out.println();
		}
	}
}

