import java.util.*;

class Q10{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter String: ");
		String str = sc.next();
		int ln = str.length();
		int count = 0;

		while(ln>0){
			count++;
			ln--;
		}

		if(count==0){
			System.out.print("String is empty");
		}
		else{
			System.out.println(str.charAt(count-1));
		}
	}
}

