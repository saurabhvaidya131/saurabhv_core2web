import java.util.*;

class Q7{
	public static void main(String s[]){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number: ");
		int num = sc.nextInt();
		int sum = 0;
		int temp = num;
		int rem = 0;

		while(num>0){
			rem=num%10;
			int sq = rem*rem;
			sum = sum+sq;
			num/=10;
		}
		if(sum==1){
			System.out.println(temp+" is a happy number");
		}
		else{
			System.out.println(temp+" is not happy number");
		}
	}
}


